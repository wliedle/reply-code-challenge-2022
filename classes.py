
import zipfile
import numpy as np

DIR = "data"

class Utils:
    @staticmethod
    def read_test_data():
        index = 0
        demons = []
        with open("{}/test.txt".format(DIR), "r") as f:
            for line in f:
                arr = line.split()
                if index == 0:
                    p = Player(int(arr[0]), int(arr[1]), int(arr[2]), int(arr[3]))
                else:
                    d = Demon(
                        int(arr[0]),
                        int(arr[1]),
                        int(arr[2]),
                        int(arr[3]),
                        list(map(int, arr[4:])),
                        index - 1
                    )
                    demons.append(d)
                index = index + 1
        return p, demons
    @staticmethod
    def read_data(filename):
        index = 0
        demons = []
        with zipfile.ZipFile("{}/{}.zip".format(DIR, filename)) as z:
            with z.open("{}.txt".format(filename)) as f:
                for line in f:
                    arr = line.split()
                    if index == 0:
                        p = Player(int(arr[0]), int(arr[1]), int(arr[2]), int(arr[3]))
                    else:
                        d = Demon(
                            int(arr[0]),
                            int(arr[1]),
                            int(arr[2]),
                            int(arr[3]),
                            list(map(int, arr[4:])),
                            index - 1
                        )
                        demons.append(d)
                    index = index + 1
        return p, demons
    @staticmethod
    def select_demon(demons, player, ignore_stamina = False):
        demon = None
        for d in demons:
            if d.dead == False and ignore_stamina == False and d.required_stamina < player.stamina:
                demon = d
                break
            elif d.dead == False and ignore_stamina == True:
                demon = d
                break
        if demon == None and ignore_stamina == False:
            demon = Utils.select_demon(demons, player, True)
        return demon
    @staticmethod
    def get_fragments_for_turn(demons, turn):
        fragments = 0
        for d in demons:
            if d.dead == True:
                try:
                    fragments = d.fragments[turn-d.killedInTurn]
                except Exception:
                    None
        return fragments
    @staticmethod
    def get_stamina_for_turn(demons, turn):
        stamina = 0
        for d in demons:
            if d.dead == True:
                if (turn - d.killedInTurn) == d.turns_to_recover:
                    stamina += d.stamina_returned
        return stamina

class Player:
    def __init__(self, stamina, stamina_max, turns, enemies):
        self.stamina = stamina
        self.stamina_max = stamina_max
        self.turns = turns
        self.enemies = enemies
        self.fragments = 0
        self.kills = 0
        self.killed_demons = []
    def add_fragments(self, fragments):
        self.fragments = self.fragments + fragments
    def decrease_enemies(self):
        self.enemies = self.enemies - 1
    def decrease_stamina(self, damage):
        new_stamina = self.stamina - damage
        if new_stamina > 0:
            self.stamina = new_stamina
        else:
            self.stamina = 0
    def increase_stamina(self, recovery):
        self.stamina = self.stamina + recovery

class Demon:
    def __init__(self, required_stamina, turns_to_recover, stamina_returned, fragment_turns, fragments, id):
        self.required_stamina = required_stamina
        self.turns_to_recover = turns_to_recover
        self.stamina_returned = stamina_returned
        self.fragment_turns = fragment_turns
        self.fragments = fragments
        self.dead = False
        self.id = id
    def kill(self, turn):
        self.dead = True
        self.killedInTurn = turn

class Game:
    def __init__(self, player, demons):
        self.turn = 0
        self.player = player
        self.demons = demons
        self.best_order_present = False
    def increase_turn(self):
        self.turn = self.turn + 1
    def start(self, print_summary = False):
        while True:
            if self.best_order_present == True:
                demon = self.demons[self.best_order[self.turn - 1]]
            else:
                demon = Utils.select_demon(self.demons, self.player)

            if print_summary == True:
                print(
                    "turn: {}, player: {} {}, demon: {} {} {}".format(
                        self.turn, self.player.stamina, self.player.fragments,
                        demon.required_stamina, demon.turns_to_recover, demon.stamina_returned
                    )
                )

            if demon != None and self.player.stamina > demon.required_stamina and self.player.enemies > 0:
                self.player.decrease_stamina(demon.required_stamina)
                demon.kill(self.turn)
                self.player.decrease_enemies()
                self.player.killed_demons.append(demon.id)

            stamina = Utils.get_stamina_for_turn(self.demons, self.turn)
            self.player.increase_stamina(stamina)
            self.player.add_fragments(Utils.get_fragments_for_turn(self.demons, self.turn))

            if self.turn == self.player.turns - 1:
                break
            self.increase_turn()
        if print_summary == True:
            self.print_output()
            self.print_fragments()
    def print_fragments(self):
        print("fragments: {}".format(self.player.fragments))
    def print_output(self):
        print("killed demons: {}".format(self.player.killed_demons))
    def write_output(self):
        None
    def calculate_best_order(self):
        optimizer = Optimizer(self.player.turns, self.player.enemies, self.demons)
        self.best_order = optimizer.calculate()
        self.best_order_present = True

class Optimizer:
    def __init__(self, turns, enemies, demons):
        self.turns = turns
        self.enemies = enemies
        self.demons = demons
        self.fragment_sums = np.zeros((enemies, turns))
    def calculate(self):
        raise NotImplementedError()
