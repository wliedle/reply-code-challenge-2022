from classes import Game, Utils

#result = Utils.read_test_data()
result = Utils.read_data("00-example")
#result = Utils.read_data("01-the-cloud-abyss")
#result = Utils.read_data("02-iot-island-of-terror")
#result = Utils.read_data("03-etheryum")
#result = Utils.read_data("04-the-desert-of-autonomous-machines")
#result = Utils.read_data("05-androids-armageddon")

g = Game(player = result[0], demons = result[1])
#g.calculate_best_order()
g.start(print_summary = True)

print("The End")

